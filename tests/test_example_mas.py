import pytest

from example_mas import mosaik

import mosaik_api


@pytest.fixture
def sim():
    sim = mosaik.ExampleMas()
    sim.init('sid', step_size=1)
    return sim


def test_sim(sim):
    assert sim.step_size == 1
    assert sim.meta == {
        'api_version': mosaik_api.__api_version__,
        'models': {
            'Agent': {
                'attrs': [],
                'params': [],
                'public': True
            }
        }
    }


def test_create(sim):
    entities = sim.create(2, 'Agent')
    assert entities == [
        {'eid': '0', 'rel': [], 'type': 'Agent'},
        {'eid': '1', 'rel': [], 'type': 'Agent'}
    ]


def test_step_get_data(sim):
    sim.create(1, 'Agent')
    sim.create(1, 'Agent')
    _ = sim.step(0, {'1.0': {'val_in': {'Agent': 1, 'b': 2}}})

    data = sim.get_data({'0.0': ['val_out', 'spam'], '1.0': ['val_out']})
    assert data == {}


def test_start_simulation():
    mas_simulator = mosaik.ExampleMas()
    mas_simulator.init(
        sid='',
    )
    mas_simulator.create(
        num=1,
        model='Agent'
    )
