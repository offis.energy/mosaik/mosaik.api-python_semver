Changelog
=========

2.4.1 - 2019-05-15
------------------

- [NEW] Introduce semantic versioning.

2.4 - 2019-02-05
----------------

- [NEW] Simulator can now be started on a different node than mosaik, using the
  remote flag "-r" and the timeout flag "-t". Mosaik can the integrate the simulator
  using the "connect" method of the simmanager.

2.3 – 2019-01-24
----------------

- [BugFix] Bugfix Tests


2.2 – 2016-02-15
----------------

- [NEW] API version 2.2: Added an optional "setup_done()" method.

- [CHANGE] API version validation: The API version is no longer an integer but
  a "major.minor" string.  The *major* part has to math with mosaiks major
  version.  The *minor* part may be lower or equal to mosaik's minor version.

- [FIX] Various minor fixes and stability improvements.


2.1 – 2014-10-24
----------------

- [NEW] Allow extra API methods to be called. See
  http://mosaik.readthedocs.org/en/2.0/mosaik-api/high-level.html#mosaik_api.Simulator

- [CHANGE] The *rel* entry in the entity description returned by *create()* is
  now optional.


2.0 – 2014-09-22
----------------

- Initial release of the mosaik 2 Sim API for Python.
